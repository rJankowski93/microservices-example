package com.rafi.microservices.account;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
//@Entity
//@Table(name = "ACCOUNTS")
public class AccountData {
	//	@Id
	protected Long id;
	protected String name;
	protected Long points;

}
