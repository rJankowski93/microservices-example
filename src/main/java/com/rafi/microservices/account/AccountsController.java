package com.rafi.microservices.account;

import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class AccountsController {

	public AccountDAO accountDAO;

	@Autowired
	public AccountsController(AccountDAO accountDAO) {
		this.accountDAO = accountDAO;
	}

	@RequestMapping("/accounts")
	public List<AccountData> accountsList() {
		return accountDAO.allAccounts();
	}

	@RequestMapping("/accounts/{accountId}")
	public AccountData accountById(@PathVariable("accountId") String accountId) {
		return accountDAO.findById(Long.parseLong(accountId));
	}
}
