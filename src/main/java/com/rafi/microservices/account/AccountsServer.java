package com.rafi.microservices.account;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.cloud.client.discovery.*;
import org.springframework.context.annotation.*;

@EnableAutoConfiguration
@EnableDiscoveryClient
@Import(AccountConfiguration.class)
public class AccountsServer {

	public static void main(String[] args) {
		System.setProperty("spring.config.name", "accounts-server");
		SpringApplication.run(AccountsServer.class, args);
	}
}