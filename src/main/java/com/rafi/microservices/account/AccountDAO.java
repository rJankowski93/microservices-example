package com.rafi.microservices.account;

import org.springframework.stereotype.*;

import java.util.*;

// TODO implements DAO
@Service
public class AccountDAO {

	public AccountData findById(Long id) {
		return new AccountData(id, "Admin", 10L);
	}

	public List<AccountData> allAccounts() {
		return Arrays.asList(new AccountData(1L, "Admin", 10L), new AccountData(2L, "Admin2", 10L), new AccountData(3L, "Admin3", 10L));
	}
}
