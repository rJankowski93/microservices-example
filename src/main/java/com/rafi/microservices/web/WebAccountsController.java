package com.rafi.microservices.web;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.*;
import org.springframework.web.bind.annotation.*;

@Controller
public class WebAccountsController {

	@Autowired
	protected WebAccountsService webAccountsService;

	public WebAccountsController(WebAccountsService webAccountsService) {
		this.webAccountsService = webAccountsService;
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.setAllowedFields("accountId");
	}

	@RequestMapping("/accounts")
	public String accountsList(Model model) {
		model.addAttribute("accounts", webAccountsService.getAccounts());
		return "accounts";
	}

	@RequestMapping("/accounts/{accountId}")
	public String accountById(Model model, @PathVariable("accountId") String accountId) {
		model.addAttribute("account",  webAccountsService.getAccountById(accountId));
		return "account";
	}
}
