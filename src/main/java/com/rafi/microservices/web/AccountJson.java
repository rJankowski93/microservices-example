package com.rafi.microservices.web;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

@JsonRootName("Account")
@Getter
@Setter
public class AccountJson {

	protected Long id;
	protected String name;
	protected Long points;

}
