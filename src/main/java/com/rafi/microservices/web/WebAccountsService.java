package com.rafi.microservices.web;

import org.springframework.beans.factory.annotation.*;
import org.springframework.cloud.client.loadbalancer.*;
import org.springframework.stereotype.*;
import org.springframework.web.client.*;

import javax.annotation.*;
import java.util.*;

@Service
public class WebAccountsService {

	@Autowired
	@LoadBalanced
	protected RestTemplate restTemplate;

	protected String serviceUrl;

	public WebAccountsService(String serviceUrl) {
		this.serviceUrl = serviceUrl.startsWith("http") ? serviceUrl
				: "http://" + serviceUrl;
	}

	public List<AccountJson> getAccounts() {
		return Arrays.asList(restTemplate.getForObject(serviceUrl + "/accounts", AccountJson[].class));
	}

	public AccountJson getAccountById(String accountId) {
		return restTemplate.getForObject(serviceUrl + "/accounts/{accountId}", AccountJson.class, accountId);
	}
}
