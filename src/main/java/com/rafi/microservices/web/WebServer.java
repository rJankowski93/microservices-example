package com.rafi.microservices.web;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.cloud.client.discovery.*;
import org.springframework.cloud.client.loadbalancer.*;
import org.springframework.context.annotation.*;
import org.springframework.web.client.*;

@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(useDefaultFilters = false)  // Disable component scanner
public class WebServer {

	public static final String ACCOUNTS_SERVICE_URL = "http://accounts-service";

	public static void main(String[] args) {
		System.setProperty("spring.config.name", "web-server");
		SpringApplication.run(WebServer.class, args);
	}

	@LoadBalanced
	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}

	/**
	 * Account service calls microservice internally using provided URL.
	 */
	@Bean
	public WebAccountsService accountsService() {
		return new WebAccountsService(ACCOUNTS_SERVICE_URL);
	}

	@Bean
	public WebAccountsController accountsController() {
		return new WebAccountsController(accountsService());  // plug in account-service
	}

	@Bean
	public HomeController homeController() {
		return new HomeController();
	}
}
